using System;
namespace DateTimeTask
{
    public class MyMethods
    {
    public static void SwitchMethod () {
      Console.WriteLine ("---Welcome to Date Time task! \n 1.Date Formate 1, \n 2.Date Formate 2 ----------");
      Console.WriteLine ("Choose Which task Would you like to Run from the Above Tasks:");
      int taskNumber = Convert.ToInt32 (Console.ReadLine ());
      switch (taskNumber) {
        case 1:
          DateTimeF();
          break;
        case 2:
          DateTimeFormate();
          break;
        default:
          Console.WriteLine ("Error you have given a invalid task number as input");
          break;
      }
      Console.WriteLine ("------------------------------------------------");
      Console.WriteLine ("Would you like to do DateTime task again (Y/N)?:");
      string TaskYesOrNo = Console.ReadLine ().ToUpper ();
      if (TaskYesOrNo == "Y") {
        SwitchLoop (TaskYesOrNo);
      Console.WriteLine ("------------------------------------------------");
      }
      TaskYesOrNo = "";
    }
    public static void SwitchLoop (string TaskYesOrNo) {
      while (TaskYesOrNo == "Y") {
        SwitchMethod ();
        TaskYesOrNo = "";
      }
    }
    public static void DateTimeF(){
      Console.WriteLine ("------Task-1 Date Formate:----");
      Console.WriteLine ("Output:");
       DateTime dt1 = new DateTime(2016, 6, 8, 11, 49, 0);
      Console.WriteLine("Complete date: "+dt1.ToString());

      DateTime dateOnly = dt1.Date;
      
      Console.WriteLine("Short Date: "+dateOnly.ToString("d"));
      
      Console.WriteLine("Display date using 24-hour clock format:");
      
      Console.WriteLine(dateOnly.ToString("g"));
      Console.WriteLine(dateOnly.ToString("MM/dd/yyyy HH:mm"));
    }

    public static void DateTimeFormate(){
      Console.WriteLine ("------Task-2 Date and Time Formate:----");
      Console.WriteLine ("Output:");
          System.DateTime moment = new System.DateTime(2016, 8, 16, 3, 57, 32, 11);

      Console.WriteLine("year = " +moment.Year);

      Console.WriteLine("month = " +moment.Month);

      Console.WriteLine("day = " +moment.Day);

      Console.WriteLine("hour = " +moment.Hour);

      Console.WriteLine("minute = " +moment.Minute);

      Console.WriteLine("second = " +moment.Second);

      Console.WriteLine("millisecond = " +moment.Millisecond);
    
    }
    }
}
    